<?php
// src/Entity/Product.php
// ...

// this use statement is needed for the annotations
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    
    
    private $description;
    
    /**
     * 
     * @return type string
     */
    function getName() {
        return $this->name;
    }
    /**
     * 
     * @return type string
     */
    function getDescription() {
        return $this->description;
    }
    /**
     * 
     * @param type $name
     */
    function setName($name) {
        $this->name = $name;
    }
    /**
     * 
     * @param type $description
     */
    function setDescription($description) {
        $this->description = $description;
    }


}