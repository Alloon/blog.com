<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;

class PostController extends Controller
{
    public function show($slug)
    {
        dump($slug);
        
        $em = $this->getDoctrine()->getManager();
        
        $post = $em->getRepository(Post::class)->getPost($slug);
        return $this->render('post/show.html.twig', ['post' => $post]);
    }
    
    public function show_list()
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository(Post::class)->getList();
        return $this->render('post/list.html.twig', ['posts' => $posts]);
    }
    
    public function edit(Request $request, $slug)
    {
        $errors = [];
        
        $em = $this->getDoctrine()->getManager();
        $Post = $em->find(Post::class, $slug);
        
        $messege = '';
        
        if($request->getMethod() === 'POST'){
            
            $data = $request->request->all();
           
            $errors = $this->Validate($data);
            
            if(!$errors){
            $Post->setName($data['name']);
            $Post->setDescription($data['description']);
            
            $em->persist($Post);
            $em->flush();
            
            return $this->redirectToRoute('post.list');
            }

        }
        
        return $this->render('post/edit.html.twig', [
            'errors' => $errors,
            'post' => $Post,
                ]);
    }
    
    public function add(Request $request)
    {
        
        $errors = [];
        $data = [
            'name' => '',
            'description' => ''
            ];
        
        if($request->getMethod() === 'POST'){
            
            $data = $request->request->all();
            
            $errors = $this->Validate($data);
            
            if(!$errors){
                $em = $this->getDoctrine()->getManager();

                $em->getRepository(Post::class)->addPost($data);

                return $this->render('post/add_success.html.twig');
            }
        }
        
        return $this->render('post/add.html.twig', [
            'errors' => $errors,
            'date' => $data
                ]);
    }
    
    public function remove($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $Post =$em->find(Post::class, $slug);
        $name = $Post->getName();
        $em->remove($Post);
        $em->flush();
        return $this->render('post/removed.html.twig', ['name' => $name]);
    }
    
    private function Validate($date){
        
        $errors = [];
        
        if(empty($date['name']) || !preg_match('/^[0-9a-zA-Zа-яА-Я\-\s]{5,50}$/u', $date['name'])){
            $errors['name'] = true;
        }
        
        if(empty($date['description'])){
            $errors['description'] = true;
        }
        
        if($date){
             return $errors;
        }
        
        return FALSE;
    }
}
