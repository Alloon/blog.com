<?php

namespace App\Controller;
use App\Entity\Page;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function index($slug)
    {
        $em = $this->getDoctrine()->getManager();
        
        $RepoPage = $em->getRepository(Page::class);
        
        $Page = $RepoPage->getPage($slug);
        
        return $this->render('page/index.html.twig',['page' => $Page]);
    }
}