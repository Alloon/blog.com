<?php
namespace App\Repository;
use App\Entity\Page;

class PageRepository extends \Doctrine\ORM\EntityRepository {
    public function GetPage($id){
        $em = $this->getEntityManager();
        return $result = $em->find(Page::class, $id);
        
    }
    public function getLinkPages(){
        
        $em = $this->getEntityManager();
        
        $query = $em->createQuery('SELECT p.id, p.name FROM App:Page p order by p.id');
        
        return $result = $query->getResult();
        
    }
}
