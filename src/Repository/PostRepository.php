<?php
namespace App\Repository;
use App\Entity\Post;

class PostRepository extends \Doctrine\ORM\EntityRepository {
    public function getPost($id){
        $em = $this->getEntityManager();
        return $result = $em->find(Post::class, $id);
        
    }
    public function getList(){
        
        $em = $this->getEntityManager();
        
        $query = $em->createQuery('SELECT p.id, p.name, p.description, p.date FROM App:Post p order by p.id DESC');
        
        $query->setMaxResults(10);
        
        return $result = $query->getResult(); 
    }
    
    public function addPost($data){
        $em = $this->getEntityManager();
        
        $Post = new Post();
        
        $Post->setDate(new \DateTime('now'));
        $Post->setName($data['name']);
        $Post->setDescription($data['description']);
        
        $em->persist($Post);
        $em->flush();
        
        return $Post;
    }
}
